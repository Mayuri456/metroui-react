import { Space, Form, Input, Button, Select } from 'antd';
import React, { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

const { Option } = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};


const Demo = () => {
const [form] = Form.useForm();

  const [visible, setVisible] = useState(false);
  const handleCancel = () => { 
    setVisible(false);
  }
  
  useEffect(() => {
 
    axios({
      method: "GET",
      
      url: `http://192.168.1.239:5511/devices/deviceid?deviceid=220   `,
    })
      .then((res) => {
        form.setFieldsValue({
          device_id: res.data.device_id,
          device_code: res.data.device_code,
          name: res.data.name,
          device_health_status: res.data.device_health_status,
          a_panel_number: res.data.a_panel_number,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    },[220]);
  
  


  const onFinish = (values) => {
    console.log(values);
  };


  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item
        name="device_id"
        label="Device ID"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="device_code"
        label="Device Code"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="name"
        label="Device Name"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="device_health_status"
        label="Health Status"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="a_panel_number"
        label="Panel Number"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Select
          placeholder="Select a option and change input text above"
          // onChange={onGenderChange}
          allowClear
        >
          <Option value="male">a</Option>
          <Option value="female">b</Option>
          <Option value="other">c</Option>
        </Select>
      </Form.Item>
      <Form.Item
        noStyle
        shouldUpdate={(prevValues, currentValues) => prevValues.gender !== currentValues.gender}
      >
        {({ getFieldValue }) =>
          getFieldValue('gender') === 'other' ? (
            <Form.Item
              name="customizeGender"
              label="Customize Gender"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
          ) : null
        }
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit"  style={{ marginLeft: "90px", background: "green" }}>
          Submit
        </Button>
        <Space/>
        {/* <Button key="back" onClick={handleCancel} style={{background: "red", color: "white"}}>
              Cancel
        </Button> */}

        {/* <Button type="primary" onClick={onFill} style={{ marginLeft: "5px"}}>
          Fill form
        </Button> */}
      </Form.Item>
    </Form>
  );
};


export default Demo;