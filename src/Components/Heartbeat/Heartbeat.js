import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import moment from "moment";
import { Space, Spin, Empty } from "antd";
const Heartbeat = () => {
  const [nodata, setNodata] = useState(false);
  function getnewdata(res) {
    try {
      if (res.data.results[0].series[0].values != undefined) {
        return res.data.results[0].series[0].values;
      }
    } catch (err) {
      setNodata(true);
    }
  }
  const { panelid } = useParams();
  const [isLoading, setIsLoading] = useState(true);

  const keyToTime = (key) => {
    var time = moment(key.substring(11, 13), ["HH"]).format("HH");
    return time;
  };
  const [data, setData] = useState([]);

  useEffect(() => {
    //API for fetching Hearbeat list of current panelid
    const date_ = moment(new Date()).format("YYYY-MM-DD");
    const panelnodata = localStorage.getItem("panelno");

    axios({
      method: "GET",
      params: {
        panel_no: `${panelnodata}`,
        start_time: `${date_}T00:00:00+05:30`,
        end_time: `${date_}T23:59:59+05:30`,
      },
      url: `/getheartbeat/panelid`,
    })
      .then((res) => {
        try {
          setData(getnewdata(res));
          setIsLoading(false);
          console.log(res.data);
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, [panelid]);

  if (isLoading) {
    return (
      <div>
        <Space size="middle">
          <Spin
            size="large"
            style={{ marginLeft: "530px", marginTop: "230px" }}
          />
        </Space>
      </div>
    );
  }
  if (nodata) {
    return <Empty />;
  }

  return (
    <>
      <table border="1px" width="100%">
        <tr>
          {data.map((hdata) => {
            return <th>{keyToTime(hdata[0])}</th>;
          })}
        </tr>
        <tr>
          {data.map((hdata) => {
            return (
              <td
                style={{
                  backgroundColor: getDisplay(hdata),
                  textAlign: "center",
                }}
              >
                {hdata[1]}
              </td>
            );
          })}
        </tr>
      </table>
    </>
  );
};
function getDisplay(hdata) {
  if (hdata[1] >= 4) {
    return "green";
  } else if (hdata[1] >= 2) {
    return "#dbba20";
  } else {
    return "red";
  }
}
export default Heartbeat;
