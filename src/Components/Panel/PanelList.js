import React, { useState, useEffect } from "react";
import { TreeSelect, Table, Button, Input, Space, message, Spin } from "antd";
import axios from "axios";
import { SearchOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import Highlighter from "react-highlight-words";
import { Link } from "react-router-dom";
// import Search from "antd/lib/transfer/search";

const PanelList = () => {
  const [value, setValue] = useState(undefined);
  const [data, setData] = useState([]);
  const [treeData, setTreeData] = useState([]);
  const [filteredPanelList, setFilteredPanelList] = useState(data);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [user, SetUser] = useState("admin");
  const [searchVal, setSearchVal] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const OrgSelection = useSelector((state) => state.OrgSelectorReducer);
  // const { Search } = Input;

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            var searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });

              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: "Panel Name",
      dataIndex: "panelname",
      key: "panelname",
      ...getColumnSearchProps("panelname"),

      sorter: (a, b) => a.panelname.length - b.panelname.length,
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Panel Number",
      dataIndex: "panelnumber",
      key: "panelnumber",
      ...getColumnSearchProps("panelnumber"),

      sorter: (a, b) => a.panelnumber - b.panelnumber,
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Site Name",
      dataIndex: "sitename",
      key: "sitename",
      ...getColumnSearchProps("sitename"),

      sorter: (a, b) => a.sitename.length - b.sitename.length,
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Area Name",
      dataIndex: "areaname",
      key: "areaname",
      ...getColumnSearchProps("areaname"),
      //filteredValue: filteredInfo.name || null,

      sorter: (a, b) => a.areaname.length - b.areaname.length,
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (a, key) => (
        <div>
          {/* {user === "admin" ? ( */}
          <Space size="middle">
            <Link to={"/paneledit/" + a.panelid}>
              <Button type="primary">Edit</Button>
            </Link>
            {/* <Popconfirm
              title="Are you sure to delete this Panel?"
              // onConfirm={confirm_delete(a.panelid)}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <Button type="danger">Delete</Button>
            </Popconfirm> */}
          </Space>
          {/* ) : null} */}
        </div>
      ),
    },
  ];

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();

    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const onChange = (e) => {
    setValue(e);
    if (!e) {
      setFilteredPanelList(data);
      return;
    }
    if (e.split("_")[0] === "site") {
      setFilteredPanelList(
        data.filter((panel) => {
          return panel.siteid == e.split("_")[1];
        })
      );
    }
    if (e.split("_")[0] === "area") {
      setFilteredPanelList(
        data.filter((panel) => {
          return panel.areaid == e.split("_")[1];
        })
      );
    }
    if (e.split("_")[0] === "region") {
      setFilteredPanelList(
        data.filter((panel) => {
          return panel.regionid == e.split("_")[1];
        })
      );
    }
    console.log(filteredPanelList);
  };

  const onFilterSearch = (e) => {
    console.log(e);
  };
  //On delete button this function called
  /*const confirm_delete = (a) => {
    axios({
      method: "POST",
      url: `/panel/deregister/panelid?panelid=${a.panelid}`,
    })
      .then((res) => {
        message.success("Panel deregistered successfully!");
      })
      .catch((err) => {
        console.log(err);
        message.success("Oops! something went wrong");
      });
  };*/

  /*function cancel(e) {
    console.log(e);
    message.error("Click on No");
  }*/
  //Delete Button code end

  useEffect(() => {
    // API for fetching panel list (all without filtering)
    axios({
      method: "GET",

      url: `/panel/orgid?org_id=${OrgSelection.current_org}`,
    })
      .then((res) => {
        setData(res.data);
        setFilteredPanelList(res.data);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });

    // API for fetching tree view data
    axios({
      method: "GET",

      url: `/getorg/treeview?org_id=${OrgSelection.current_org}`,
    })
      .then((res) => {
        const treeFormatData = res.data.data.map((region) => {
          return {
            key: "region_" + region.regionid,
            title: region.name,
            children: region.areas.map((area) => {
              return {
                key: "area_" + area.areaid,
                title: area.name,
                children: area.sites.map((site) => {
                  return {
                    key: "site_" + site.siteid,
                    title: site.sitename,
                  };
                }),
              };
            }),
          };
        });

        setTreeData(treeFormatData);
        // setIsLoading2(false);
        setValue(undefined);
        // console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [OrgSelection.current_org]);

  if (isLoading) {
    return (
      <div>
        <Space size="middle">
          <Spin
            size="large"
            style={{ marginLeft: "530px", marginTop: "230px" }}
          />
        </Space>
      </div>
    );
  }

  return (
    <>
      <div className="filters">
        <h1 style={{ marginTop: "6px" }}>Sites:</h1>

        <TreeSelect
          showSearch
          style={{
            width: "20%",
            alignItems: "left",
            alignSelf: "left",
            paddingLeft: "10px",
          }}
          value={value}
          dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
          placeholder="Select Panel"
          allowClear
          //   treeDefaultExpandAll
          onChange={onChange}
          treeData={treeData}
          onSearch={onFilterSearch}
          treeIcon
        />
        <div style={{ paddingLeft: "10px" }}>
          <Link to="/paneladd">
            <Button type="primary">
              {/* <PlusOutlined /> */}
              Add
            </Button>
          </Link>
        </div>
        {/* Temp comment */}
        {/* <div style={{ paddingLeft: "10px" }}>
          <Input.Search
            onChange={(e) => setSearchVal(e.target.value)}
            placeholder="Search"
            enterButton
          />
        </div> */}
      </div>
      <br />
      {/* <div
        className="site-layout-background"
        style={{ padding: 24, minHeight: 360 }}
      > */}
      <Table
        columns={columns}
        dataSource={filteredPanelList}
        size="middle"
        pagination={{
          defaultPageSize: 20,
          showSizeChanger: true,
          pageSizeOptions: [10, 25, 50],
        }}
        scroll={{ y: 400 }}
      />
      {/* </div> */}
    </>
  );
};

export default PanelList;
