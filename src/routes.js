import PanelList from "./Components/Panel/PanelList";
import PanelEdit from "./Components/Panel/PanelEdit";
import PanelAdd from "./Components/Panel/PanelAdd";
import Dashboard1 from "./Components/dash/Dashboard";
import Dashboard from "./Dashboard/Dashboard";

const routes = [
  { path: "/panel", name: "Panel", component: PanelList },
  { path: "/", name: "Dashboard", component: Dashboard },
];
export default routes;
