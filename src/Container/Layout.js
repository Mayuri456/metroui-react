import React, { useState, useEffect } from "react";
import { DesktopOutlined } from "@ant-design/icons";
import { Menu, Layout } from "antd";
import logo from "../Container/logo.svg";
import "../Container/layout.css";
import TheContent from "./TheContent";
import { Select, PageHeader } from "antd";
import { useHistory } from "react-router-dom";
import { org_Selection, set_current_org } from "../Actions/OrgSelection";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
// const { Text} = Typography;
const { Option } = Select;
const { Sider, Footer } = Layout;
const { SubMenu } = Menu;

const TheLayout = () => {
  let history = useHistory();
  const OrgSelection = useSelector((state) => state.OrgSelectorReducer);
  const dispatcher = useDispatch();
  const [curren_org_name, setCurrentOrg] = useState(undefined);
  const onChange = (value, key) => {
    setCurrentOrg(value);
    dispatcher(set_current_org(key.key));

    localStorage.setItem(
      "current_selected_org",
      JSON.stringify({ organisationid: key.key, organisationname: value })
    );
    if (window.location.pathname !== "/panel") {
      history.push("/panel");
      console.log("");
    }
  };
  const [collapsed, setCollapsed] = useState(false);
  const [user, SetUser] = useState("admin");
  const onCollapse = (e) => {
    console.log(collapsed);
    setCollapsed(!collapsed);
  };

  useEffect(() => {
    // API for fetching panel list (all without filtering)
    axios({
      method: "GET",
      headers: {
        Authorization: localStorage.getItem("JWTtoken"),
      },
      url: `/getallorgs`,
    })
      .then((res) => {
        console.log(res.data);

        const current_org = JSON.parse(
          localStorage.getItem("current_selected_org")
        );
        if (current_org) {
          dispatcher(
            org_Selection({
              org_list: res.data,
              current_org: current_org.organisationid,
              curren_org_name: current_org.organisationname,
            })
          );
          setCurrentOrg(current_org.organisationname);
        } else {
          dispatcher(
            org_Selection({
              org_list: res.data,
              current_org: res.data[0].organisationid,
              curren_org_name: res.data[0].organisationname,
            })
          );
          setCurrentOrg(res.data[0].organisationname);
        }

        // dispatcher(set_current_org(res.data[0].organisationid ));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <>
      <Layout>
        <Sider trigger={null} collapsed={collapsed} onCollapse={onCollapse}>
          <div className="logo1">
            <img src={logo} height="80px" width="65px" />
          </div>

          <div className="logo">
            {" "}
            <p className="l">Welcome User </p>{" "}
          </div>
          <Menu theme="dark" mode="inline">
            <SubMenu key="sub2" icon={<DesktopOutlined />} title="Home">
              {/* <Link to="/panel" key="b">
                <Menu.Item key="3" icon={<GatewayOutlined />}>
                  Panels
                </Menu.Item>
              </Link> */}
            </SubMenu>
          </Menu>
        </Sider>
        <Layout className="site-layout" style={{ minWidth: "100vh" }}>
          <TheContent />
          <Footer
            style={{
              textAlign: "center",
              backgroundColor: "gray",
              position: 0,
            }}
          >
            Integrated Active Monitoring Pvt. Ltd.
          </Footer>
        </Layout>
      </Layout>
    </>
  );
};
export default TheLayout;
